Installation:

1. Se till att du har python, version 2.7.5 eller högre, installerat.
    https://www.python.org/downloads/

2. Installera pip (package manager): https://pip.pypa.io/en/latest/installing.html

3. Ladda ner scripten: https://bitbucket.org/quesebifurcan/jsv-score/downloads

4. ...och placera dem "någonstans".
sedan (i en terminal):

   `cd "någonstans/jsv-score"`

   `pip install -r requirements.txt`
   
5. Kör:
 
   `python image_test.py`