# -*- coding: utf-8 -*-
# Importera json-modulen (inkluderad i python).
# Att "importera" en modul betyder att dess funktioner och klasser blir tillgängliga för oss.
import json

# Vi använder oss av scikit-image för att identifiera punkterna.
# Dokumentation/projecktbeskrivning hittar man här: http://scikit-image.org/
# Koden nedan är en förenklad version av det här exemplet: http://scikit-image.org/docs/stable/auto_examples/plot_blob.html

# Här importerar vi sub-moduler och funktioner från scikit-image -- scikit-image är ett stort bibliotek, så det är viktigt att vi bara plockar det som vi behöver.
from skimage import io
from skimage.feature import blob_doh
from skimage.color import rgb2gray

# Läs in en bild.
image = io.imread('/Users/fred/Downloads/dots.jpg')
# Konvertera till gråskala.
image_gray = rgb2gray(image)
# Se http://scikit-image.org/docs/stable/api/skimage.feature.html#skimage.feature.blob_doh för dokumentation av blob_doh.
blobs_doh = blob_doh(image_gray, max_sigma=10, threshold=.01)

# Initialisera en lista som vi kan använda för att samla upp våra punkter.
points = []
for blob in blobs_doh:
    # Det här är ett lite speciellt python-idiom. Det kallas
    # "tuple-unpacking" och innebär att du kan sätta två eller
    # fler variabler på samma gång. "blob_doh" returnerar nämligen
    # inte ett (1) värde, utan tre.
    # Om vi lägger till ett "print"-statement här:
    # print blob
    # så får vi följande resultat:
    # [364 206  10]
    # [371 539   4]
    # [372 359   2]
    # [372 515   2]
    # [372 562   2]
    # [498 806   7]
    # [503 854   2]
    # [547 888   2]
    # [547 915   2]
    # [547 963   2]
    # [547 997   2]
    # [1747 1234   10]
    # M.a.o.: variablerna x, y och r (radien) kommer att korrespondera med
    # elementen i listorna ovan.
    y, x, r = blob
    # Här sparar vi alla värden i ett s.k. "dictionary". Kanske den 
    # allra bästa datastrukturen i hela världen :-)
    # Se https://docs.python.org/2/tutorial/datastructures.html#dictionaries
    # för mer info.
    points.append({"x": x, "y": y, "r": r})

# Sortera våra punkter enligt deras x-värde -- vi arbetar ju med sekventiell
# data. "lambda" är en s.k. anonym funktion, som "sorted" använder sig av för
# att veta *hur* listans element ska sorteras.
points = sorted(points, key=lambda x: x.get('x'))

# Slutligen skriver vi vår "points"-lista till disk.
# Om du kör det här programmet, så måste du ändra den första delen:
# '/Users/fred/Downloads/dots-data.json'
# ...så att den beskriver platsen där du vill ha din fil.
with open('/Users/fred/Downloads/dots-data.json', 'w') as outfile:
    json.dump(points, outfile)

# Exempel-output:
# [{"y": 364, "x": 206, "r": 10},
#  {"y": 372, "x": 359, "r": 2},
#  {"y": 372, "x": 515, "r": 2},
#  {"y": 371, "x": 539, "r": 4},
#  {"y": 372, "x": 562, "r": 2},
#  {"y": 498, "x": 806, "r": 7},
#  {"y": 503, "x": 854, "r": 2},
# ...
# etc.
