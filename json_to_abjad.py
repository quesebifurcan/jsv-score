import json
from abjad import *

def normalize(value, min_, max_):
    return (value - min_) / (max_ - min_)

# Open file with json-encoded data. 
with open('/Users/fred/Downloads/dots-data.json', 'r') as infile:
    points = json.load(infile)

# Get the offsets and scale them by "scalar". 
# Values correspond to millisecond offsets.
offsets = [0] + [point['x'] for point in points]
scalar = 100
scaled_offsets = [x * scalar for x in offsets]

# Get pitches and normalize them. 
# N.B. Contour is currently inverted: "high" is "low" and vice versa.
pitches = [point['y'] for point in points]
min_pitch, max_pitch = float(min(pitches)), float(max(pitches))
pitches = [int(normalize(p, min_pitch, max_pitch) * 100) for p in pitches[:-1]]

# Quantize.
sequence = quantizationtools.QEventSequence.from_millisecond_offsets(
    scaled_offsets)
quantizer = quantizationtools.Quantizer()
result = quantizer(sequence)

# The first tie-chain is a rest; change its notehead.
first_tie_selector = selectortools.select_first_logical_tie_in_pitched_runs()
for tie in first_tie_selector(result):
    for note in tie[:]:
        override(note).note_head.style = 'harmonic-black'

# Set the pitches of the remaining notes.
rest_tie_selector = selectortools.select_all_but_first_logical_tie_in_pitched_runs()
for pitch, tie in zip(pitches, rest_tie_selector(result)):
    for note in tie[:]:
        note.written_pitch = pitch

# Render score (requires Lilypond)
score = Score([Staff([result])])
show(score)
